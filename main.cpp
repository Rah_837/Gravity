#include "GravityApp.h"

int main()
{
	GravityApp app;

	//app.create(sf::Style::None, {800, 600}, "Dravity");
	app.create(sf::Style::Fullscreen);

	app.run();

	return 0;
}