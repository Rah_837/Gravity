#include "Entity.h"

Entity::Entity(const sf::Vector2f& pos, float radius, const sf::Vector2f& acceleration)
	: acceleration(acceleration)
{
	setPosition(pos);
	setRadius(radius);
}

Entity::~Entity() {}