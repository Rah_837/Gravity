#pragma once

#include <SFML\Graphics.hpp>
#include <list>

struct Entity : sf::CircleShape
{
	Entity(const sf::Vector2f& = {0, 0}, float = 0.1, const sf::Vector2f& = { 0, 0 });
	virtual ~Entity();

	sf::Vector2f	acceleration;
};