#include "Application.h"

Application::Application() : m_timeSpeed(1) {}

Application::~Application() {}


void Application::create(sf::Uint32 style, sf::VideoMode mode, sf::String title)
{
	m_window.create(style == sf::Style::Fullscreen ? sf::VideoMode::getDesktopMode() : mode, title, style);
	m_window.setVerticalSyncEnabled(true);
}

void Application::close()
{
	finish();
	m_window.close();
}

void Application::run()
{
	start();

	while (m_window.isOpen())
	{
		sf::Event event;
		while (m_window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed || event.key.code == sf::Keyboard::Escape)
				close();

			eventHandler(event);
		}

		m_window.clear();
		update();
		m_window.display();
	}
}