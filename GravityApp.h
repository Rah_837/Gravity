#pragma once

#include "Application.h"
#include "Entity.h"

#define G 0.000000000066740831
#define FACTOR 1000000

class GravityApp final : public Application
{
private:
	void start() override;
	void update() override;
	void eventHandler(const sf::Event&) override;
	void finish() override;

	std::list<Entity> m_entities;
};