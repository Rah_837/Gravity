#include "GravityApp.h"

void GravityApp::start()
{
	m_timeSpeed = pow(10, 2);

	for (int i = 0; i < 128; ++i)
		m_entities.emplace_back(sf::Vector2f(rand() % m_window.getSize().x, rand() % m_window.getSize().y), 1);

	/*m_entities.emplace_back(sf::Vector2f(0, 303), 81);
	m_entities.emplace_back(sf::Vector2f(81, 102), 1, sf::Vector2f(0.0085, 0));*/

	/*m_entities.emplace_back(sf::Vector2f(0, 284), 100);
	m_entities.emplace_back(sf::Vector2f(1166, 284), 100);*/
}

void GravityApp::update()
{
	for (sf::Uint16 i = 0; i < m_timeSpeed; ++i)
	{
		for (auto it1 = m_entities.begin(); it1 != m_entities.end(); ++it1)
		{
			Entity& ent1 = *it1;
			const sf::Vector2f& pos1 = ent1.getPosition();
			float rad1 = ent1.getRadius();

			for (auto it2 = m_entities.begin(); it2 != m_entities.end();)
			{
				if (it1 == it2)
				{ ++it2; continue; }

				Entity& ent2 = *it2;
				float rad2 = ent2.getRadius();
				const sf::Vector2f& pos2 = ent2.getPosition();

				float distX = (pos1.x + rad1) - (pos2.x + rad2);
				float distY = (pos1.y + rad1) - (pos2.y + rad2);
				float dist = sqrt(pow(distX, 2) + pow(distY, 2));

				float gravity = G * FACTOR * rad1 * rad2 / pow(dist, 2);
				float force1 = gravity / rad1;
				float force2 = gravity / rad2;
				int dirX = pos1.x + rad1 > pos2.x + rad2 ? -1 : 1;
				int dirY = pos1.y + rad1 > pos2.y + rad2 ? -1 : 1;

				ent1.acceleration += { force1 * dirX, force1 * dirY };
				ent2.acceleration += { force2 * -dirX, force2 * -dirY };

				if (rad1 + rad2 >= dist)
				{
					ent1.setPosition(rad1 <= rad2 ? sf::Vector2f(pos2.x - rad1, pos2.y - rad1)
						: sf::Vector2f(pos1.x - rad2, pos1.y - rad2));
					float radSum = rad1 + rad2;
					ent1.acceleration = {	(rad1 * ent1.acceleration.x + rad2 * ent2.acceleration.x) / radSum,
											(rad1 * ent1.acceleration.y + rad2 * ent2.acceleration.y) / radSum };
					ent1.setRadius(radSum);
					rad1 = radSum;

					it2 = m_entities.erase(it2);
				}
				else
					++it2;
			}

			ent1.setPosition(pos1 + ent1.acceleration);
			ent1.setPointCount(rad1 < 10 ? 10 : rad1);
			sf::Uint16 color = rad1 * 2;
			ent1.setFillColor(sf::Color(color > 255 ? 255 : color, 255 - color < 0 ? 0 : 255 - color, 0));
		}
	}

	for (auto ent : m_entities)
		m_window.draw(ent);
}

void GravityApp::eventHandler(const sf::Event& event)
{
	if (event.type == sf::Event::KeyReleased)
	{
		if (event.key.code == sf::Keyboard::Space)
		{
			finish();
			start();
		}
	}
}

void GravityApp::finish()
{ m_entities.clear(); }